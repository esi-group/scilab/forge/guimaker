mode(-1)
// Demo from scilab
pg=list();
//pg($+1)=list(list(1));
pg($+1)=list(list([1 2]),list([4 2],'pushbutton','$\scalebox{2}{\LaTeX\ x^2}$','callback','messagebox(''''LaTeX is beautiful'''','''''''',''''info'''')'),...
             list([1 2]),list([4 2],'pushbutton','<mrow><mtext>MathML</mtext><mphantom><mi>a</mi></mphantom><msup><mi>x</mi><mn>2</mn></msup></mrow>','callback','messagebox(''''MathML is beautiful'''','''''''',''''info'''')'),list([1 2]));
pg($+1)=list(list([1 2]));
pg($+1)=list(list([2 2]),list([5 5],'text','$\text{Text: }\Gamma(s)=\int_0^\infty t^{s-1}\mathrm{e}^{-t}\,\mathrm{d}t$','fontsize',15),list([2 2]));
guimaker(pg,list('guimaker demo with LaTeX',350,15),[],2);

